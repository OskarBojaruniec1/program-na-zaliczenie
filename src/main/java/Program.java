import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Program {

    public static boolean isLogged = false;
    public static boolean onRun = true;
    public static boolean inMethod = false;

    private static String userLogin;
    public static int price = 0;
    public static ArrayList<String> cart = new ArrayList<>();

    public static File userDB = new File("src/main/DBfiles/database.csv");
    public static File menu = new File("src/main/DBfiles/menu.csv");
    public static File deliveryHistory = new File("src/main/DBfiles/delivery_history.csv");


    public static void main(String[] args) {

        if (!userDB.exists()) {
            createDB();
            createMenu();
        }
        while (onRun) {

            if (isLogged) {
                menuLogged();
            } else {
                menuMain();
            }
        }
    }

    public static void createDB() {

        try {

            userDB.getParentFile().mkdirs();
            userDB.createNewFile();
            menu.createNewFile();
            deliveryHistory.createNewFile();

        } catch (IOException e) {

            System.out.println("Błąd z bazą danych, spróbuj uruchomić program ponownie");
        }
    }

    public static void createMenu() {

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(menu, true));

            bw.write("1;Margherita;(sos, ser);(⌀ 24 cm;20 zł);(⌀ 40 cm;31 zł)");
            bw.newLine();
            bw.write("2;Funghi;(sos, ser, pieczarki);(⌀ 24 cm;22 zł);(⌀ 40 cm;33 zł)");
            bw.newLine();
            bw.write("3;Prosciutto;(sos, ser, szynka);(⌀ 24 cm;24 zł);(⌀ 40 cm;35 zł)");
            bw.newLine();
            bw.write("4;Salami;(sos, ser, salami, cebula);(⌀ 24 cm;26 zł);(⌀ 40 cm;36 zł)");
            bw.newLine();
            bw.write("5;Romana;(sos, ser, szynka wędzona, cebula, tabasco);(⌀ 24 cm;27 zł);(⌀ 40 cm;40 zł)");
            bw.newLine();
            bw.write("6;Capricciosa;(sos, ser, szynka, pieczarki);(⌀ 24 cm;25 zł);(⌀ 40 cm;39 zł)");
            bw.newLine();
            bw.write("7;Hawaii;(sos, ser, szynka, ananas);(⌀ 24 cm;26 zł);(⌀ 40 cm;36 zł)");
            bw.newLine();
            bw.write("8;Maffia;(sos, ser, szynka, szynka wędzona, cebula, pieczarki);(⌀ 24 cm;27 zł);(⌀ 40 cm;42 zł)");
            bw.newLine();
            bw.write("9;Wiejska;(sos, ser, boczek, kiełbasa, cebula, ogórek, chrzan);(⌀ 24 cm;26 zł);(⌀ 40 cm;41 zł)");
            bw.newLine();
            bw.write("10;Szparagowa;(sos, ser, kurczak, brokuły, pomidor, szparagi);(⌀ 24 cm;25 zł);(⌀ 40 cm;39 zł)");
            bw.newLine();
            bw.flush();
            bw.close();

        } catch (IOException e) {

            System.out.println("Błąd z bazą danych, spróbuj uruchomić program ponownie");
        }

    }

    public static void menuMain() {

        Scanner scChoose = new Scanner(System.in);
        System.out.println("Witamy w naszej pizzeri, jak możemy ci pomóc?");
        System.out.println
                ("1 - Zaloguj\n" +
                        "2 - Załóż konto\n" +
                        "3 - Exit");

        String choose = scChoose.nextLine();

        switch (choose) {
            case "1" -> login();
            case "2" -> createAcc();
            case "3" -> exit();
            default -> System.out.println("Niepoprawna komenda, spróbuj ponownie");
        }
    }

    public static void login() {

        Scanner scInput = new Scanner(System.in);

        System.out.println("Podaj login");
        String login = scInput.nextLine();

        System.out.println("Podaj hasło");
        String password = scInput.nextLine();

        checkLoginInputsInDB(login, password);

    }

    public static void checkLoginInputsInDB(String login, String password) {

        Scanner scFile = null;

        try {
            scFile = new Scanner(userDB);
        } catch (FileNotFoundException e) {
            System.out.println("Błąd z bazą danych, spróbuj uruchomić program ponownie");
        }

        boolean equals = false;

        while (scFile.hasNextLine()) {

            String line = scFile.nextLine();
            String[] lineToParts = line.split(" ", 2);

            if (login.equals(lineToParts[0]) && password.equals(lineToParts[1])) {

                equals = true;
                userLogin = login;

            }
        }

        if (equals) {

            isLogged = true;
            System.out.println("Zalogowano!");
        } else {
            System.out.println("Podany login lub hasło jest nieprawidłowe");
        }
    }

    public static void createAcc() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Podaj login");
        String login = sc.nextLine();

        while (checkIsLoginHasWhiteSpaces(login) || checkIsLoginUsed(login)) {

            login = sc.nextLine();
        }

        System.out.println("Podaj hasło");
        String password = sc.nextLine();
        while (checkStrengthOfPassword(password)) {

            System.out.println("Podaj hasło");
            password = sc.nextLine();
        }
        addToDB(login, password);
    }

    public static boolean checkIsLoginHasWhiteSpaces(String login) {

        char[] loginChars = login.toCharArray();

        for (char c : loginChars) {

            if (Character.isWhitespace(c)) {

                System.out.println("Pole login nie może zawierać spacji, spróbuj ponownie");
                return true;
            }
        }
        return false;
    }

    public static boolean checkIsLoginUsed(String login) {

        Scanner scUserDB = null;

        try {
            scUserDB = new Scanner(userDB);
        } catch (FileNotFoundException e) {

            System.out.println("Błąd z bazą danych, spróbuj uruchomić program ponownie");
        }

        while (scUserDB.hasNextLine()) {

            String line = scUserDB.nextLine();
            String[] lineToParts = line.split(" ", 2);

            if (login.equals(lineToParts[0])) {

                System.out.println("Podany login jest już zajęty, spróbuj ponownie");
                return true;
            }
        }
        return false;
    }

    public static boolean checkStrengthOfPassword(String password) {

        Pattern compiledPattern = Pattern.compile("^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)(?=.*\\W).{8,32}$");
        Matcher matcher = compiledPattern.matcher(password);

        if (matcher.matches()) {
            return false;
        } else {
            System.out.println("Hasło jest za słabe (powinno zawierać przynajmniej od 8 do 32 znaków w tym: duże i małe litery, cyfry, znaki specjalne)");
            return true;
        }
    }

    public static void addToDB(String login, String password) {

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(userDB, true));

            bw.write(login + " " + password);
            bw.newLine();
            bw.flush();
            bw.close();

        } catch (IOException e) {

            System.out.println("Błąd z bazą danych, spróbuj uruchomić program ponownie");
        }
    }

    public static void exit() {

        onRun = false;
    }

    public static void menuLogged() {

        Scanner scChoose = new Scanner(System.in);
        System.out.println("Co byś chciał zrobić?");
        System.out.println
                ("1 - Wyloguj\n" +
                        "2 - Złóż zamówienie\n" +
                        "3 - Twoje konto");

        String choose = scChoose.nextLine();

        switch (choose) {
            case "1" -> logout();
            case "2" -> {
                inMethod = true;
                displayPizzaMenu();
                createOrder();

            }
            case "3" -> accountInfo();
            default -> System.out.println("Niepoprawna komenda, spróbuj ponownie");
        }
    }

    public static void createOrder() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Podaj proszę numer pizzy którą chciałbyś zamówić: ");
        String userInputNumberOfPizza = sc.nextLine();

        if (addToCart(userInputNumberOfPizza)) {
            System.out.println("Niepoprawny numer pizzy spróbuj ponownie");
            createOrder();
        }

        System.out.println("Twoje zamówienie:\n" + cart + "\n" + price + " zł");

        while (inMethod) {
            System.out.println("Czy chcesz zamówić coś jeszcze?\n1 - Tak\n2 - Nie, przejdźmy do podania adresu\n3 - Zrezygnuj z zamawiania");
            String choose = sc.nextLine();

            switch (choose) {
                case "1" -> createOrder();
                case "2" -> {
                    inMethod = false;
                    deliveryAdress();
                }
                case "3" -> {
                    price = 0;
                    cart.clear();
                    inMethod = false;
                }
                default -> System.out.println("Niepoprawna komenda");
            }
        }
    }

    public static void displayPizzaMenu() {

        Scanner scMenu = null;

        try {
            scMenu = new Scanner(menu);
        } catch (FileNotFoundException e) {
            System.out.println("Błąd z bazą danych, spróbuj uruchomić program ponownie");
        }

        while (scMenu.hasNextLine()) {

            String line = scMenu.nextLine();
            String[] lineToParts = line.split(";", 7);
            System.out.println(Arrays.toString(lineToParts));
        }
    }

    public static boolean addToCart(String userInputPizza) {

        Scanner scMenu = null;

        try {
            scMenu = new Scanner(menu);
        } catch (FileNotFoundException e) {
            System.out.println("Błąd z bazą danych, spróbuj uruchomić program ponownie");
        }

        Scanner scInputSizeOfPizza = new Scanner(System.in);
        boolean correctInput = true;

        while (scMenu.hasNextLine()) {

            String line = scMenu.nextLine();
            String[] lineToParts = line.split(";", 7);

            if (userInputPizza.equals(lineToParts[0])) {

                while (correctInput) {
                    System.out.println("Podaj wielkość\n1 - 24cm\n2 - 40cm");
                    String size = scInputSizeOfPizza.nextLine();
                    switch (size) {
                        case "1" -> {

                            cart.add(lineToParts[1] + " mała");
                            priceToInt(lineToParts[4]);
                            correctInput = false;
                        }
                        case "2" -> {

                            cart.add(lineToParts[1] + " duża");
                            priceToInt(lineToParts[6]);
                            correctInput = false;
                        }
                        default -> System.out.println("Nie poprawna komenda");
                    }
                }
                break;
            }
        }
        return correctInput;
    }

    public static void priceToInt(String lineToPart) {

        String priceAsString = "";
        char[] lineToPartChars = lineToPart.toCharArray();

        for (char c : lineToPartChars) {

            if (Character.isDigit(c)) {
                priceAsString += String.valueOf(c);
            }
        }
        int currentPrice = Integer.valueOf(priceAsString);
        price += currentPrice;

    }

    public static void deliveryAdress() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Podaj proszę miasto");
        String city = sc.nextLine();

        System.out.println("Podaj proszę ulice");
        String street = sc.nextLine();

        System.out.println("Podaj numer domu/mieszkania*");
        String numberOfStreet = sc.nextLine();

        summary(city, street, numberOfStreet);
    }

    public static void summary(String city, String street, String numberOfStreet) {

        inMethod = true;
        Scanner scChoose = new Scanner(System.in);

        System.out.println("\nAdres dostawy:\n" + "Miasto: " + city + "\n" + "Ulica: " + street
                + " " + numberOfStreet + "\n");
        System.out.println("Zamówienie:\n" + cart + "\n");
        System.out.println("Do zapłacenia " + price + " zł gotówką przy odbiorze\n");
        System.out.println("Czas oczekiwania 60 minut\n");

        addToDeliveryHistory(cart, price, street, numberOfStreet, userLogin);

        while (inMethod) {

            System.out.println("Co byś chciał dalej zrobić?");
            System.out.println("1 - Powrót do menu głównego");
            String choose = scChoose.nextLine();
            if (choose.equals("1")) {

                cart.clear();
                price = 0;
                inMethod = false;
                break;

            } else {
                System.out.println("Niepoprawna komenda");
            }
        }
    }

    public static void addToDeliveryHistory(ArrayList<String> cart, int price, String street, String numberOfStreet, String userLogin) {

        Date now = new Date();

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(deliveryHistory, true));

            bw.write(userLogin + " " + now + " " + cart + " " + price + "zł " + street + " " + numberOfStreet);
            bw.newLine();
            bw.flush();
            bw.close();
        } catch (IOException e) {

            System.out.println("Błąd z bazą danych, spróbuj uruchomić program ponownie");
        }
    }

    public static void accountInfo() {

        Scanner sc = new Scanner(System.in);

        System.out.println("Wybierz opcje która cię interesuje\n1 - Historia zamówień\n2 - Zmień hasło\n3 - Wróć");
        String choose = sc.nextLine();

        switch (choose) {

            case "1" -> displayDeliveryHistory();
            case "2" -> changePassword();
            case "3" -> menuLogged();
            default -> {
                System.out.println("Niepoprawna komenda");
                accountInfo();
            }
        }
    }

    public static void displayDeliveryHistory() {

        inMethod = true;
        Scanner scDH = null;

        try {
            scDH = new Scanner(deliveryHistory);
        } catch (IOException e) {

            System.out.println("Błąd z bazą danych, spróbuj uruchomić program ponownie");
        }

        Scanner scInput = new Scanner(System.in);

        while (scDH.hasNextLine()) {

            String line = scDH.nextLine();
            String[] lineToParts = line.split(" ");
            if (userLogin.equals(lineToParts[0])) {

                line = line.replace(userLogin, "Zamówienie z dnia:");
                System.out.println(line);
            }
        }

        while (inMethod) {

            System.out.println("\nCo byś chciał dalej zrobić?");
            System.out.println("1 - Wróć");
            String choose = scInput.nextLine();
            if (choose.equals("1")) {

                inMethod = false;
                accountInfo();
                break;

            } else {
                System.out.println("Niepoprawna komenda");
            }
        }
    }

    public static void changePassword() {

        Scanner scInDB = null;

        try {
            scInDB = new Scanner(userDB);
        } catch (IOException e) {

            System.out.println("Błąd z bazą danych, spróbuj uruchomić program ponownie");
        }

        StringBuffer sb = new StringBuffer();
        File f = new File("src/main/DBfiles/database.txt");

        while (scInDB.hasNextLine()) {

            String line = scInDB.nextLine();
            String[] lineToParts = line.split(" ", 2);

            if (userLogin.equals(lineToParts[0])) continue;

            sb.append(line);
            sb.append("\n");

            try {
                PrintWriter pw = new PrintWriter(new FileOutputStream(f, false));
                pw.print(sb.toString());
                pw.close();
            } catch (IOException e) {

                System.out.println("Błąd z bazą danych, spróbuj uruchomić program ponownie");
            }
        }

        Scanner scUserInput = new Scanner(System.in);
        System.out.println("Podaj nowe hasło");
        String password = scUserInput.nextLine();

        while (checkStrengthOfPassword(password)) {

            System.out.println("Podaj nowe hasło");
            password = scUserInput.nextLine();
        }

        addToDB(userLogin, password);
    }

    public static void logout() {

        isLogged = false;
        userLogin = "";
        System.out.println("Wylogowano!");
    }

}
